<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
      return [
            'username'             => 'oprator',
            'password'             => bcrypt('2647187'),
            'isAdmin'               => 0,
        'remember_token'      => str_random(10),
    ];
});

$factory->define(App\Personel::class, function (Faker $faker) {
      return [
            'name'               => $faker->name,
            'father_name'     => $faker->lastName,
            'personal_id'       => $faker->numberBetween(11122233 , 99999999) ,
            'rank_id'             => $faker->numberBetween(1 , 6) ,
            'organ_id'           =>$faker->numberBetween(1 , 3),
            'department'       =>$faker->city,
      ];
});

$factory->define(App\Soldier::class, function (Faker $faker) {
      return [
            'name'               => $faker->name,
            'father_name'     => $faker->lastName,
            'national_id'       => $faker->unique()->numberBetween(1122334455 , 9999999999) ,
            'rank_id'             => $faker->numberBetween(1 , 6) ,
            'organ_id'           =>$faker->numberBetween(1 , 3),
            'department'       =>$faker->city,
      ];
});

$factory->define(App\Letter::class, function (Faker $faker) {
      return [
            'number'             => $faker->unique()->numberBetween(0 , 1300),
            'date'                  => $faker->date(),
            'time'                  => $faker->time() ,
            'oprator_name'     => '$facker' ,
            'model'                =>  $faker->numberBetween(1 , 6),
            'person_id'           =>  factory('App\Personel')->create()->id,
            'explanation'        => $faker->paragraph(1),
            'isDelete'              => false,
      ];
});

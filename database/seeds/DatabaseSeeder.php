<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
      /**
       * Seed the application's database.
       *
       * @return void
       */
      public function run()
      {
            factory(App\User::class, true)->create();

            DB::table('ranks')->insert(array(
                  [ 'name' => 'سرباز وظیفه',       'isPersonel' => false , 'isSoldier' => true],
                  [ 'name' => 'دانشجو وظیفه',    'isPersonel' => false , 'isSoldier' => true],
                  [ 'name' => 'گروهبانسوم',        'isPersonel' => false , 'isSoldier' => true],
                  [ 'name' => 'گروهباندوم',         'isPersonel' => false , 'isSoldier' => true],
                  [ 'name' => 'گروهبانیکم',        'isPersonel' => true , 'isSoldier' => true],
                  [ 'name' => 'کارمند',               'isPersonel' => true , 'isSoldier' => false],
                  [ 'name' => 'استوار دوم',         'isPersonel' => true , 'isSoldier' => true],
                  [ 'name' => 'استوار یکم',        'isPersonel' => true , 'isSoldier' => true],
                  [ 'name' => 'ستوان سوم',       'isPersonel' => true , 'isSoldier' => true],
                  [ 'name' => 'ستوان دوم',        'isPersonel' => true , 'isSoldier' => true],
                  [ 'name' => 'ستوان یکم',       'isPersonel' => true , 'isSoldier' => true],
                  [ 'name' => 'سروان',             'isPersonel' => true , 'isSoldier' => false],
                  [ 'name' => 'سرگرد',             'isPersonel' => true , 'isSoldier' => false],
                  [ 'name' => 'سرهنگ دوم',      'isPersonel' => true , 'isSoldier' => false],
                  [ 'name' => 'سرهنگ',            'isPersonel' => true , 'isSoldier' => false],
                  [ 'name' => 'سرتیپ دوم',      'isPersonel' => true , 'isSoldier' => false],
                  [ 'name' => 'سرتیپ',            'isPersonel' => true , 'isSoldier' => false],
            ));
            DB::table('organs')->insert(array(
                  ['name' => 'نیروی هوایی' , 'type' => 'فرمانده محترم نیروی هوایی'],
                  ['name' => 'منطقه پدافند' , 'type' => 'فرمانده محترم منطقه پدافند'],
                  ['name' => 'گروه پدافند' , 'type' => 'فرمانده محترم گروه پدافند'],
                  ['name' => 'تایپ' , 'type' => ''],
            ));


            factory(App\Personel::class, 25)->create();

            factory(App\Soldier::class, 25)->create();

            factory(App\Letter::class, 50)->create();
      }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLettersTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('letters', function (Blueprint $table) {
                  $table->increments('id');
                  $table->smallInteger('number');
                  $table->date('date');
                  $table->time('time');
                  $table->char('oprator_name' , 100);
                  $table->tinyInteger('model' );
                  $table->mediumInteger('person_id');
                  $table->string('explanation');
                  $table->boolean('isDelete');
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('letters');
      }
}

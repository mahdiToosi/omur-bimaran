<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSoldiersTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('soldiers', function (Blueprint $table) {
                  $table->increments('id');
                  $table->char('name');
                  $table->char('father_name');
                  $table->bigInteger('national_id')->unique();
                  $table->integer('rank_id');
                  $table->tinyInteger('organ_id');
                  $table->char('department');
                  $table->timestamps();

            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('soldiers');
      }
}

<!doctype html>
<html lang="fa" dir="rtl">
<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>letters</title>
      <link rel="stylesheet" href="{{asset('css/foundation.css')}}">
      <link rel="stylesheet" href="{{asset('css/styles.css')}}">

</head>
<body>
<div id="app">

      <app></app>

</div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('isPersonel') ? 'has-error' : ''}}">
    {!! Form::label('isPersonel', 'Ispersonel', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('isPersonel', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('isPersonel', '0', true) !!} No</label>
</div>
    {!! $errors->first('isPersonel', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('isSoldier') ? 'has-error' : ''}}">
    {!! Form::label('isSoldier', 'Issoldier', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('isSoldier', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('isSoldier', '0', true) !!} No</label>
</div>
    {!! $errors->first('isSoldier', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

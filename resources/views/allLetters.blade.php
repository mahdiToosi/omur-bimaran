<!doctype html>
<html lang="fa" dir="rtl">
<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Form</title>

      <link rel="icon" href="./favicon.png">

      <link rel="stylesheet" href="./css/foundation.css">
      <link rel="stylesheet" href="./css/styles.css">

</head>
<body>
      <div id="app">

            <dispatchtohospital></dispatchtohospital>

      </div>
<script src="./js/app.js"></script>

</body>
</html>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('father_name') ? 'has-error' : ''}}">
    {!! Form::label('father_name', 'Father Name', ['class' => 'control-label']) !!}
    {!! Form::text('father_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('father_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('personal_id') ? 'has-error' : ''}}">
    {!! Form::label('personal_id', 'Personal Id', ['class' => 'control-label']) !!}
    {!! Form::number('personal_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('personal_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('rank_id') ? 'has-error' : ''}}">
    {!! Form::label('rank_id', 'Rank Id', ['class' => 'control-label']) !!}
    {!! Form::number('rank_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('rank_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('organ') ? 'has-error' : ''}}">
    {!! Form::label('organ', 'Organ', ['class' => 'control-label']) !!}
    {!! Form::number('organ', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('organ', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('department') ? 'has-error' : ''}}">
    {!! Form::label('department', 'Department', ['class' => 'control-label']) !!}
    {!! Form::text('department', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('department', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

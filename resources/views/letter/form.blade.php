<div class="form-group {{ $errors->has('number') ? 'has-error' : ''}}">
    {!! Form::label('number', 'Number', ['class' => 'control-label']) !!}
    {!! Form::number('number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    {!! Form::label('date', 'Date', ['class' => 'control-label']) !!}
    {!! Form::date('date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('time') ? 'has-error' : ''}}">
    {!! Form::label('time', 'Time', ['class' => 'control-label']) !!}
    {!! Form::input('time', 'time', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('time', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('oprator_name') ? 'has-error' : ''}}">
    {!! Form::label('oprator_name', 'Oprator Name', ['class' => 'control-label']) !!}
    {!! Form::text('oprator_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('oprator_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('model') ? 'has-error' : ''}}">
    {!! Form::label('model', 'Model', ['class' => 'control-label']) !!}
    {!! Form::number('model', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('person_id') ? 'has-error' : ''}}">
    {!! Form::label('person_id', 'Person Id', ['class' => 'control-label']) !!}
    {!! Form::number('person_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('person_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('explanation') ? 'has-error' : ''}}">
    {!! Form::label('explanation', 'Explanation', ['class' => 'control-label']) !!}
    {!! Form::text('explanation', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('explanation', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('isDelete') ? 'has-error' : ''}}">
    {!! Form::label('isDelete', 'Isdelete', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('isDelete', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('isDelete', '0', true) !!} No</label>
</div>
    {!! $errors->first('isDelete', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

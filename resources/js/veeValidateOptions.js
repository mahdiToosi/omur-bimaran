import VeeValidate from 'vee-validate';
Vue.use(VeeValidate, {  classes: true  });
import { Validator } from 'vee-validate';

const dictionary = {
      fa: {
            messages: {
                  numeric: 'عدد باید وارد شود',
                  required: 'این فیلد حتما باید پر شود',
                  min: 'کافی نیست',
                  max: 'اضافی وارد شده'
            }
      }
};
// Override and merge the dictionaries
Validator.localize(dictionary);
const validator = new Validator({ first_name: 'alpha' });
validator.localize('fa');
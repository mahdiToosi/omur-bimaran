require('./bootstrap');

import { store } from './store/store';

import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)

require('./VuePersianDate')

require('./veeValidateOptions')

require('./VueComponentsRegister')

import Home from './components/Home'
import dispatchtohospital from './components/Forms/dispatchtohospital'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const router = new VueRouter({
      mode: 'history',
      routes: [
            {path: '/', component: Home , name: 'Home'},
            {path: '/printLetter/:personelOrSoldier/:formModel/:personCode',
                        component: dispatchtohospital , name: 'printDispatchtohostpital'}
      ],
});


const app = new Vue({
      el: '#app',
      store,
      router
});

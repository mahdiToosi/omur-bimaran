
Vue.component('app', require('./components/app.vue').default);
Vue.component('Home', require('./components/Home.vue').default);

Vue.component('firstForm', require('./components/Forms/firstForm.vue').default);
Vue.component('dispatchtohospital', require('./components/Forms/dispatchtohospital.vue').default);


Vue.component('A4', require('./components/Forms/papers/A4.vue').default);
Vue.component('A5', require('./components/Forms/papers/A5.vue').default);
Vue.component('EzamLashkar', require('./components/Forms/papers/EzamLashkar.vue').default);
Vue.component('EzamTehran', require('./components/Forms/papers/EzamTehran.vue').default);
Vue.component('EzamBimarestan', require('./components/Forms/papers/EzamBimarestan.vue').default);
Vue.component('Esterahat', require('./components/Forms/papers/Esterahat.vue').default);
Vue.component('MorajeatTehran', require('./components/Forms/papers/MorajeatTehran.vue').default);

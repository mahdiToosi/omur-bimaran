import VuePersianDatetimePicker from 'vue-persian-datetime-picker'
Vue.use(VuePersianDatetimePicker, {
      name: 'custom-date-picker',
      props: {
            format: "YYYY-MM-DD",
            displayFormat: 'jYYYY/jMM/jDD',
            editable: false,
            inputClass: 'form-control mini-input',
            color: '#62a8ca ',
            autoSubmit: false,
      }
});
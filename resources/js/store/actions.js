export const  getData = ({commit} , data ) => {
      let addr = '/backend/printLetter/'+ data.PorS +'/'+ data.FormModel +'/'+ data.Pcode
      // console.log(addr)
      axios.get(addr)
            .then((response) => {
                  commit('setData', response.data)
                  // console.log(response.data)
            })
};


export const  updateName = ({commit} , val ) => {
      commit('setName' , val)
};
export const  updateOrgan = ({commit} , val ) => {
      commit('setOrgan' , val)
};
export const  updateRank = ({commit} , val ) => {
      commit('setRank' , val)
};
export const  updateNumber = ({commit} , val ) => {
      commit('setNumber' , val)
};
export const  updateDepartment = ({commit} , val ) => {
      commit('setDepartment' , val)
};
export const  updateFathername = ({commit} , val ) => {
      commit('setFathername' , val)
};
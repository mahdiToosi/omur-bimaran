import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import * as mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
      state:{
            rankOptions:[
                  {
                  "id": 1,
                  "name": "سرباز وظیفه",
                  "isPersonel": 0,
                  "isSoldier": 1,
                  },
                  {
                  "id": 2,
                  "name": "دانشجو وظیفه",
                  "isPersonel": 0,
                  "isSoldier": 1,
                  },
                  {
                  "id": 3,
                  "name": "گروهبانسوم",
                  "isPersonel": 0,
                  "isSoldier": 1,
                  },
                  {
                  "id": 4,
                  "name": "گروهباندوم",
                  "isPersonel": 0,
                  "isSoldier": 1,
                  },
                  {
                  "id": 5,
                  "name": "گروهبانیکم",
                  "isPersonel": 1,
                  "isSoldier": 1,
                  },
                  {
                  "id": 6,
                  "name": "کارمند",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  },
                  {
                  "id": 7,
                  "name": "استوار دوم",
                  "isPersonel": 1,
                  "isSoldier": 1,
                  },
                  {
                  "id": 8,
                  "name": "استوار یکم",
                  "isPersonel": 1,
                  "isSoldier": 1,
                  },
                  {
                  "id": 9,
                  "name": "ستوان سوم",
                  "isPersonel": 1,
                  "isSoldier": 1,
                  },
                  {
                  "id": 10,
                  "name": "ستوان دوم",
                  "isPersonel": 1,
                  "isSoldier": 1,
                  },
                  {
                  "id": 11,
                  "name": "ستوان یکم",
                  "isPersonel": 1,
                  "isSoldier": 1,
                  },
                  {
                  "id": 12,
                  "name": "سروان",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  },
                  {
                  "id": 13,
                  "name": "سرگرد",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  },
                  {
                  "id": 14,
                  "name": "سرهنگ دوم",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  
                  
                  },
                  {
                  "id": 15,
                  "name": "سرهنگ",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  },
                  {
                  "id": 16,
                  "name": "سرتیپ دوم",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  },
                  {
                  "id": 17,
                  "name": "سرتیپ",
                  "isPersonel": 1,
                  "isSoldier": 0,
                  }
            ] ,
            organOptions: [
                  { name:  'نیروی هوایی' , type: 'felan', id: 1},
                  { name:  'منطقه پدافند' , type: 'felan', id: 2},
                  { name:  'گروه پدافند' , type: 'felan', id: 3},
                  { name:  'تایپ' , type: 'felan', id: 4},
            ],
            hospitalsOptions: [
                  { name : 'بیمارستان لشکر' , situation :'lashkar' , type: ''},
                  { name : 'بیمارستان ثامن' , situation :'samen' , type: ''},
                  { name : 'بیمارستان امام حسین' , situation :'emamHosein' , type: ''},
                  { name : 'بیمارستانی دیگر' , situation :'other' , type: ''},
            ],
            backendData :{
                  "number": null,
                  "opratorName": null,
                  "formmodel": null,
                  "personalcode": null,
                  "name": null,
                  "fathername": null,
                  "rank": null,
                  "organ": null,
                  "department": null,
            }
      },
      getters:{
            rankOptionsSoldier: (state) => {
                  let ranks = []
                  state.rankOptions.forEach(val => {
                        if (val.isSoldier == 1) {
                              ranks.push(val)
                        }
                  });
                  return ranks
            },
            rankOptionsPersonel: (state) => {
                  let ranks = []
                  state.rankOptions.forEach(val => {
                        if (val.isPersonel == 1) {
                              ranks.push(val)
                        }
                  });
                  return ranks
            },
            organOptions: (state) => {
                  return state.organOptions
            },
            hospitalsOptions: (state) => {
                  return state.hospitalsOptions
            },
            backendData: (state) => {
                  return state.backendData
            },


            getName: (state) => {
                  return state.backendData.name
            },
            getOrgan: (state) => {
                  return state.backendData.organ
            },
            getRank: (state) => {
                  return state.backendData.rank
            },
            getNumber: (state) => {
                  return state.backendData.number
            },
            getDepartment: (state) => {
                  return state.backendData.department
            },
            getFathername: (state) => {
                  return state.backendData.fathername
            },
      },
      mutations,
      actions
});

export const setData = (state , data ) =>{
      state.backendData = data;
};


export const setName = (state , val ) =>{
      state.backendData.name = val;
};
export const setOrgan = (state , val ) =>{
      state.backendData.organ = val;
};
export const setRank = (state , val ) =>{
      state.backendData.rank = val;
};
export const setNumber = (state , val ) =>{
      state.backendData.number = val;
};
export const setDepartment = (state , val ) =>{
      state.backendData.department = val;
};
export const setFathername = (state , val ) =>{
      state.backendData.fathername = val;
};
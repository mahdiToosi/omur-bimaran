<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Letter;
use App\Personel;
use App\Soldier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LetterController extends Controller
{
    public $lettersNum = [
            'esterahatPezeshki'     => 1,
            'ezamBeBimarestan'    => 2,
            'ezamBeTehran'          => 3,
            'morajeatAzTehran'     => 4
    ];

    public function exportData($personelOrSoldier , $formModel , $personCode)
    {
            $data = [
                "number"      =>  null,                 "opratorName"  =>    null,
                "formmodel" =>  $formModel,    "personalcode"  =>    $personCode,
                "name"          =>     null,              "department"        =>     null,
                "fathername" =>  null,                 "rank"                =>     null,
                "organ"          => null,                 
                
            ];

//            select person with personal code if exist
            if($personelOrSoldier == 'personel'){
                $person = Personel::where('personal_id', $personCode)->first();
            }elseif ($personelOrSoldier == 'soldier'){
                $person = Soldier::where('national_id', $personCode)->first();
            }else{
                return abort(404);
            }
            if ($person){
                $data['name'] = $person->name;
                $data['fathername'] = $person->father_name;
                $data['department'] = $person->department;
                $data['organ'] = $person->organ;
                $data['rank'] = $person->rank;
            }

//            select oprator name
//            delete below line on production ************************************************
            Auth::loginUsingId(1 , true);
            if (Auth::check()){
                $data['opratorName'] = Auth::user()->username;
            }

//            select the last number of the letter + 1
            $thatKindLetter = $this->lettersNum[$formModel];
            $lastLetter = Letter::where('model' , $thatKindLetter)
                                                ->orderBy('number' , 'desc')->first();
            $newNumberForLetter = $lastLetter->number + 1 ;
            $data['number'] = $newNumberForLetter;


            // dd($data);

            return $data;
    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $letter = Letter::where('number', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('time', 'LIKE', "%$keyword%")
                ->orWhere('oprator_name', 'LIKE', "%$keyword%")
                ->orWhere('model', 'LIKE', "%$keyword%")
                ->orWhere('person_id', 'LIKE', "%$keyword%")
                ->orWhere('explanation', 'LIKE', "%$keyword%")
                ->orWhere('isDelete', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $letter = Letter::latest()->paginate($perPage);
        }

        return view('letter.index', compact('letter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('letter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Letter::create($requestData);

        return redirect('letter')->with('flash_message', 'Letter added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $letter = Letter::findOrFail($id);

        return view('letter.show', compact('letter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $letter = Letter::findOrFail($id);

        return view('letter.edit', compact('letter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $letter = Letter::findOrFail($id);
        $letter->update($requestData);

        return redirect('letter')->with('flash_message', 'Letter updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Letter::destroy($id);

        return redirect('letter')->with('flash_message', 'Letter deleted!');
    }
}

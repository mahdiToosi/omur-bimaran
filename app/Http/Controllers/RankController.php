<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Rank;
use Illuminate\Http\Request;

class RankController extends Controller
{
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\View\View
       */
      public function sendRanksOption()
      {
            $rankOptions = Rank::all();

            return $rankOptions;
      }
      public function index(Request $request)
      {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                  $rank = Rank::where('name', 'LIKE', "%$keyword%")
                        ->orWhere('isPersonel', 'LIKE', "%$keyword%")
                        ->orWhere('isSoldier', 'LIKE', "%$keyword%")
                        ->latest()->paginate($perPage);
            } else {
                  $rank = Rank::latest()->paginate($perPage);
            }

            return view('rank.index', compact('rank'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\View\View
       */
      public function create()
      {
            return view('rank.create');
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function store(Request $request)
      {

            $requestData = $request->all();

            Rank::create($requestData);

            return redirect('rank')->with('flash_message', 'Rank added!');
      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function show($id)
      {
            $rank = Rank::findOrFail($id);

            return view('rank.show', compact('rank'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function edit($id)
      {
            $rank = Rank::findOrFail($id);

            return view('rank.edit', compact('rank'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function update(Request $request, $id)
      {

            $requestData = $request->all();

            $rank = Rank::findOrFail($id);
            $rank->update($requestData);

            return redirect('rank')->with('flash_message', 'Rank updated!');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function destroy($id)
      {
            Rank::destroy($id);

            return redirect('rank')->with('flash_message', 'Rank deleted!');
      }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'letters';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'date', 'time', 'oprator_name', 'model', 'person_id', 'explanation', 'isDelete'];

    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personels';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'father_name', 'personal_id', 'rank_id', 'organ', 'department'];

    public function rank()
    {
        return $this->belongsTo('App\Rank' , 'rank_id');
    }
    
    public function organ()
    {
        return $this->belongsTo('App\Organ' , 'organ_id');
    }
    
}
